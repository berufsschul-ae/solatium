# Solatium
[![pipeline status](https://gitlab.com/berufsschul-ae/solatium/badges/master/pipeline.svg)](https://gitlab.com/berufsschul-ae/solatium/commits/master)
[![coverage report](https://gitlab.com/berufsschul-ae/solatium/badges/master/coverage.svg)](https://gitlab.com/berufsschul-ae/solatium/commits/master)

Solatium is a program that simulates an ATM. The users can login into their accounts and check their balance, send transactions and disburse and deposit their money.

## Features
- Login with Verification
- Persistent Account Balance
- Deposit & disburse
- All in a Blockchain (with validation)

## Setup
Download one of the newest builds and run it with `java -jar filename.jar`

## Setup with Docker
This project can be executed with Docker. To do so pull the image from `registry.gitlab.com/berufsschul-ae/solatium:latest`.<br>
To start the application run `docker run --name solatium -p 8080:8080 registry.gitlab.com/berufsschul-ae/solatium:latest`. <br>
To persist the transactions you have to mount a volume to `/tmp/persistence`.<br>
There is also a docker-compose file with all the config done, just start it with `docker-compose -f docker-compose.example.yml up` and take a look at `localhost`.<br>

## How to use
Go to the root of you server it will ask you for a login
Currently there are only the following hardcoded users:<br>
"user" with password "foo"<br>
"user1" with password "bar"<br>
"blazej" with password "blazej123"<br>
"augusto" with password "augusto123"<br>
"bjarne" with password "bjarne123"<br>
"Bank" with password "Bank"<br>
"heiko" with password "heiko123"<br>

After a successful login you can see your balance. You can also send the transaction and disburse and deposit your money.

## API
You can also use the included API to check for the balance of a user or send transaction etc.

The API has the following schema:
`/api/(action)?(params)`

You still need to be authorized to access this API.
We suggest using curl for this. Here are the request made with curl and some explanations.

### Checking the balance
`curl -u user:foo localhost:8080/api/balance`<br>
If you'd run the code locally, your curl request could look like this.<br>
This request uses the balance action to the balance for the user specified after the curl parameter "-u".
After the parameter we need to provide the user credentials with the following schema "(username):(password)".

### Sending transactions
`curl -u user:foo "localhost:8080/api/transaction?receiverId=user1&amount=100"`<br>
This request requires an receiverId and the amount as parameters. Here we are sending a transaction from user to user1 with an amount of 100.

### Depositing money
`curl -u user:foo localhost:8080/api/deposit?amount=100`
Deposit only takes one parameter and that's amount.

### Disburse money
`curl -u user:foo localhost:8080/api/disburse?amount=100`
Disburse also only takes one parameters; amount.

## Setup for development
Clone the project and do a `mvn clean install`<br>
Run the project with `mvn spring-boot:run`
