FROM openjdk:8-jdk-alpine


ARG FILE_PATH

COPY $FILE_PATH /tmp/app.jar
COPY entrypoint.sh /entrypoint.sh

EXPOSE 8080
ENTRYPOINT ["sh", "entrypoint.sh"]
