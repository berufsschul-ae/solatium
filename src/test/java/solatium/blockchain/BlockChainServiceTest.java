package solatium.blockchain;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.*;

public class BlockChainServiceTest {

    @Test
    public void shouldAddToChain() {
        String hash = "foo123";
        Transaction transaction = new Transaction("0", "0", 0);
        Block block = new TransactionBlock("", transaction, new Date().getTime());
        HashedBlock hashedBlock = new HashedBlock(block, hash);
        Chain blockChain = new BlockChainService();
        blockChain.addToChain(hashedBlock);
        assertEquals(hash, blockChain.getLastHash());
    }

    @Test
    public void shouldValidateChain() {
        Chain blockChain = new BlockChainService();
        for (int i = 0; i < 10; i++) {
            String previousHash = blockChain.getLastHash();
            blockChain.addToChain(getHashedBlock(previousHash));
        }
        assertTrue(blockChain.validateChain());
    }

    @Test
    public void shouldFailValidateChain() {
        Transaction transaction = new Transaction("0", "0", 0);
        Block wrongBlock = new TransactionBlock("", transaction, new Date().getTime());
        Chain blockChain = new BlockChainService();
        for (int i = 0; i < 10; i++) {
            String previousHash = blockChain.getLastHash();
            blockChain.addToChain(getHashedBlock(previousHash));
        }
        blockChain.addToChain(wrongBlock.getHashed());
        assertFalse(blockChain.validateChain());
    }

    @Test
    public void shouldReturnListWithBlocks() {
        Chain blockChain = new BlockChainService();
        Transaction transaction = new Transaction("fff", "xxx", 10);
        Block block = new TransactionBlock(blockChain.getLastHash(), transaction, new Date().getTime());
        blockChain.addToChain(block.getHashed());
        List<HashedBlock> hashedBlocks = blockChain.getBlockChain();
        assertEquals(2, hashedBlocks.size());
        assertEquals("Transaction{senderName='0', receiverName='0', value=0}", hashedBlocks.get(0).getBlock().getData().toString());
        assertEquals("Transaction{senderName='fff', receiverName='xxx', value=10}", hashedBlocks.get(1).getBlock().getData().toString());
    }

    @Test
    public void shouldToString() {
        Chain blockChain = new BlockChainService();
        Transaction transaction = new Transaction("0", "0", 0);
        Block block0 = new TransactionBlock(blockChain.getLastHash(), transaction, 1);
        Block block1 = new TransactionBlock(blockChain.getLastHash(), transaction, 2);
        Block block2 = new TransactionBlock(blockChain.getLastHash(), transaction, 3);
        blockChain.addToChain(block0.getHashed());
        blockChain.addToChain(block1.getHashed());
        blockChain.addToChain(block2.getHashed());
        assertEquals("Chain{\n" +
                "HashedBlock{hash='2B0A03DE90CC5AAEE0AF05ABC1B68739416AB185B822A4B54586CEABCD9CE18A', block=Block{previousHash='0', data='Transaction{senderName='0', receiverName='0', value=0}', timestamp=0}}\n" +
                "HashedBlock{hash='54AA7114A20D2C6B0A0AC9582FC9427D68F17C1DB0919C63F61FE8A5EA06A344', block=Block{previousHash='2B0A03DE90CC5AAEE0AF05ABC1B68739416AB185B822A4B54586CEABCD9CE18A', data='Transaction{senderName='0', receiverName='0', value=0}', timestamp=1}}\n" +
                "HashedBlock{hash='4F3D56D9A87A64382D36FFD9FFBB216CDE1B543BC9195F31EC6D5EAF3F3AF2AF', block=Block{previousHash='2B0A03DE90CC5AAEE0AF05ABC1B68739416AB185B822A4B54586CEABCD9CE18A', data='Transaction{senderName='0', receiverName='0', value=0}', timestamp=2}}\n" +
                "HashedBlock{hash='6938106F7D731217E044E6051F95A132A29B255F6C1733D4C0AF5F077BBBA686', block=Block{previousHash='2B0A03DE90CC5AAEE0AF05ABC1B68739416AB185B822A4B54586CEABCD9CE18A', data='Transaction{senderName='0', receiverName='0', value=0}', timestamp=3}}\n" +
                "}", blockChain.toString());

    }

    @Test
    public void shouldReturnGenesisBlock() {
        Chain blockChain = new BlockChainService();
        Transaction genesisTransaction = new Transaction("0", "0", 0);
        Block genesisBlock = new TransactionBlock("0", genesisTransaction, 0);
        HashedBlock hashedGenesisBlock = genesisBlock.getHashed();

        assertEquals(hashedGenesisBlock.toString(), blockChain.getGenesisBlock().toString());
    }

    @Test (expected = BlockChainStartsNotWithGenesisBlockException.class)
    public void shouldThrowExceptionByMissingGenesisBlock() {
        List<HashedBlock> blockList = new ArrayList<>();
        blockList.add(new HashedBlock(new FakeBlock(), "foo"));
        new BlockChainService(blockList);
        fail();
    }

    @Test (expected = BlockChainStartsNotWithGenesisBlockException.class)
    public void shouldThrowExceptionByNotLeadingGenesisBlock() {
        HashedBlock genesisBlock = new BlockChainService().getGenesisBlock();
        HashedBlock hashedBlock = new HashedBlock(new FakeBlock(), "foo");
        List<HashedBlock> blockList = Arrays.asList(hashedBlock, genesisBlock);
        new BlockChainService(blockList);
        fail();
    }

    @Test
    public void shouldReturnEqualList() {
        HashedBlock genesisBlock = new BlockChainService().getGenesisBlock();
        HashedBlock hashedBlock = new HashedBlock(new FakeBlock(), "foo");
        List<HashedBlock> blockList = Arrays.asList(genesisBlock, hashedBlock);
        Chain blockChain = new BlockChainService(blockList);
        assertEquals(blockList, blockChain.getBlockChain());
    }

    private HashedBlock getHashedBlock(String previousHash) {
        Transaction transaction = new Transaction("0", "0", 0);
        Block block = new TransactionBlock(previousHash, transaction, new Date().getTime());
        return block.getHashed();
    }

    private static class FakeBlock implements Block {

        @Override
        public String getDataToHash() {
            throw new RuntimeException("missing fake implementation");
        }

        @Override
        public HashedBlock getHashed() {
            throw new RuntimeException("missing fake implementation");
        }

        @Override
        public String getPreviousHash() {
            return "fake implementation";
        }

        @Override
        public Transaction getData() {
            throw new RuntimeException("missing fake implementation");
        }

        @Override
        public long getTimestamp() {
            return 0;
        }
    }

}
