package solatium.blockchain;

import org.junit.Test;

import static org.junit.Assert.*;

public class Sha256HashTest {

    @Test
    public void shouldHash() {
        Sha256Hash sha256Hash = new Sha256Hash("foo bar", "previousHash");
        assertEquals("F4CBE5D152805EF599B3649EF546D317F0897A94CBA00C8BC032F82A1FE01D7F", sha256Hash.getHash());
    }

    @Test
    public void shouldHashEmptyString() {
        Sha256Hash sha256Hash = new Sha256Hash("", "");
        assertEquals("E3B0C44298FC1C149AFBF4C8996FB92427AE41E4649B934CA495991B7852B855", sha256Hash.getHash());
    }

    @Test
    public void shouldHashNull() {
        Sha256Hash sha256Hash = new Sha256Hash(null, null);
        assertEquals("2C7BDDAFA6F824CB0E682091AA1D9CA392883CB1F5BCFF95389ADC9FEAE77FCD", sha256Hash.getHash());
    }

}
