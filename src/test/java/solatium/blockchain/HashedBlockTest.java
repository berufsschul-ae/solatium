package solatium.blockchain;

import org.junit.Test;

import static org.junit.Assert.*;

public class HashedBlockTest {

    @Test
    public void shouldToString() {
        Transaction transaction = new Transaction("0", "0", 0);
        Block block = new TransactionBlock("previous hash", transaction, 100L);
        HashedBlock hashedBlock = new HashedBlock(block, "XXXX");
        assertEquals("HashedBlock{hash='XXXX', block=Block{previousHash='previous hash', data='Transaction{senderName='0', receiverName='0', value=0}', timestamp=100}}", hashedBlock.toString());
    }

}
