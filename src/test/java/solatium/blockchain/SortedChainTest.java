package solatium.blockchain;

import org.junit.Test;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class SortedChainTest {

    private static final String GENESIS_HASH = "0";
    private final HashedBlock genesisBlock;

    public SortedChainTest() {
        Transaction genesisTransaction = new Transaction("0", "0", 0);
        Block genesisBlock = new TransactionBlock(GENESIS_HASH, genesisTransaction, 0);
        this.genesisBlock = genesisBlock.getHashed();
    }

    @Test
    public void shouldSortAHashedBlockList() {

        String hash = "foo123";
        Transaction transaction = new Transaction("0", "0", 0);
        Block block = new TransactionBlock(genesisBlock.getHash(), transaction, new Date().getTime());
        HashedBlock hashedBlock1 = new HashedBlock(block, hash);

        String hash1 = "bar123";
        Transaction transaction1 = new Transaction("0", "0", 0);
        Block block1 = new TransactionBlock(hash, transaction1, new Date().getTime());
        HashedBlock hashedBlock2 = new HashedBlock(block1, hash1);

        List<HashedBlock> blockChain = Arrays.asList(genesisBlock, hashedBlock1, hashedBlock2);
        List<HashedBlock> unsortedBlockChain = Arrays.asList(hashedBlock1, genesisBlock, hashedBlock2);

        SortedChain sortedChain = new SortedChain(unsortedBlockChain);

        assertEquals(blockChain, sortedChain.sort());
    }
}
