package solatium.blockchain;

import org.junit.Test;

import static org.junit.Assert.*;

public class BlockTest {

    @Test
    public void shouldToString() {
        Transaction transaction = new Transaction("0", "0", 0);
        Block block = new TransactionBlock("previous hash", transaction, 100L);
        assertEquals("Block{previousHash='previous hash', data='Transaction{senderName='0', receiverName='0', value=0}', timestamp=100}", block.toString());
    }

}
