package solatium.blockchain;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import solatium.blockchain.persistence.PersistBlockChainToDisk;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PersistenceBlockChainIT {

    private static final String TEST_RESOURCE_PATH = "./src/test/resources/persistenceTest/";
    private final File directory;

    public PersistenceBlockChainIT() {
        Path path = Paths.get(TEST_RESOURCE_PATH);
        directory = new File(path.toUri());
    }

    @Before
    public void setUp() {
        if (!directory.isDirectory()) {
            directory.mkdir();
        }
        assertTrue(directory.isDirectory());
    }

    @After
    public void cleanUp() throws IOException {
        FileUtils.cleanDirectory(directory);
    }

    @Test
    public void shouldWriteTheTransactionsIntoPersistence() {
        PersistBlockChainToDisk persistBlockChainToDisk = new PersistBlockChainToDisk(directory);
        List<HashedBlock> unsortedHashedBlockList = persistBlockChainToDisk.read();
        List<HashedBlock> sortedHashedBlockList = new SortedChain(unsortedHashedBlockList).sort();

        BlockChainService blockChainService = new BlockChainService(sortedHashedBlockList, persistBlockChainToDisk);
        assertEquals(1, directory.listFiles().length);

        Transaction transaction = new Transaction("user", "user1", 1);
        blockChainService.addToChain(transaction);
        assertEquals(2, directory.listFiles().length);
    }

    @Test
    public void shouldReadValidBlockChainFromFiles() {
        PersistBlockChainToDisk persistBlockChainToDisk = new PersistBlockChainToDisk(directory);

        BlockChainService blockChainService = new BlockChainService(new ArrayList<>(), persistBlockChainToDisk);

        add2Transactions(blockChainService);

        persistBlockChainToDisk = new PersistBlockChainToDisk(directory);
        List<HashedBlock> unsortedHashedBlockList = persistBlockChainToDisk.read();
        List<HashedBlock> sortedBlockList = new SortedChain(unsortedHashedBlockList).sort();

        assertTrue(new BlockChainService(sortedBlockList).validateChain());
    }

    @Test
    public void shouldReadSameBlockChainFromFiles() {
        PersistBlockChainToDisk persistBlockChainToDisk = new PersistBlockChainToDisk(directory);
        BlockChainService blockChainService = new BlockChainService(new ArrayList<>(), persistBlockChainToDisk);

        add2Transactions(blockChainService);

        List<HashedBlock> originalBlockList = blockChainService.getBlockChain();

        persistBlockChainToDisk = new PersistBlockChainToDisk(directory);
        List<HashedBlock> unsortedHashedBlockList = persistBlockChainToDisk.read();
        List<HashedBlock> sortedBlockList = new SortedChain(unsortedHashedBlockList).sort();

        assertEquals(originalBlockList.toString(), sortedBlockList.toString());
    }

    private void add2Transactions(BlockChainService blockChainService) {
        Transaction transaction = new Transaction("user", "user1", 1);
        blockChainService.addToChain(transaction);
        Transaction transaction1 = new Transaction("user1", "user", 1);
        blockChainService.addToChain(transaction1);
    }
}
