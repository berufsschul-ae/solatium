package solatium.blockchain.persistence;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import solatium.blockchain.BlockChainService;
import solatium.blockchain.HashedBlock;
import solatium.blockchain.Transaction;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Slf4j
public class LoadBlockChainIT {

    private static final String TEST_RESOURCE_PATH = "./src/test/resources/persistenceTest/";
    private final File directory;

    public LoadBlockChainIT() {
        Path path = Paths.get(TEST_RESOURCE_PATH);
        directory = new File(path.toUri());
    }

    @Before
    public void setUp() {
        if (!directory.isDirectory()) {
            directory.mkdir();
        }
        assertTrue(directory.isDirectory());
    }

    @After
    public void cleanUp() throws IOException {
        FileUtils.cleanDirectory(directory);
    }

    @Test
    public void shouldLoadCorrect() {
        PersistBlockChainToDisk persistBlockChainToDisk = new PersistBlockChainToDisk(directory);

        BlockChainService blockChainService = new BlockChainService(new ArrayList<>(), persistBlockChainToDisk);
        add2Transactions(blockChainService);
        List<HashedBlock> originalBlockList = blockChainService.getBlockChain();

        LoadBlockChain loadBlockChain = new LoadBlockChain(persistBlockChainToDisk);
        List<HashedBlock> hashedBlockList = loadBlockChain.load();

        assertEquals(originalBlockList.toString(), hashedBlockList.toString());
    }

    @Test(expected = BlockChainValidationException.class)
    public void shouldThrowExceptionByInvalidChain() {
        PersistBlockChainToDisk persistBlockChainToDisk = new PersistBlockChainToDisk(directory);

        BlockChainService blockChainService = new BlockChainService(new ArrayList<>(), persistBlockChainToDisk);
        add2Transactions(blockChainService);

        blockChainService = new BlockChainService(new ArrayList<>(), persistBlockChainToDisk);
        add2Transactions(blockChainService);

        LoadBlockChain loadBlockChain = new LoadBlockChain(persistBlockChainToDisk);

        try {
            loadBlockChain.load();
            fail();
        } catch (BlockChainValidationException e) {
            assertEquals("The block chain is corrupted", e.getMessage());
            throw e;
        }
    }

    private void add2Transactions(BlockChainService blockChainService) {
        Transaction transaction = new Transaction("user", "user1", 1);
        blockChainService.addToChain(transaction);
        Transaction transaction1 = new Transaction("user1", "user", 1);
        blockChainService.addToChain(transaction1);
    }
}
