package solatium.blockchain.persistence;

import org.junit.Test;

import static org.junit.Assert.*;

public class PersistBlockChainToVoidTest {

    @Test
    public void shouldReturnEmpty() {
        PersistBlockChain persistBlockChain = new PersistBlockChainToVoid();
        assertTrue(persistBlockChain.read().isEmpty());
    }
}
