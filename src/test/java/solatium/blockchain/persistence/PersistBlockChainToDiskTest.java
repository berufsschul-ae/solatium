package solatium.blockchain.persistence;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import solatium.blockchain.Block;
import solatium.blockchain.HashedBlock;
import solatium.blockchain.Transaction;
import solatium.blockchain.TransactionBlock;

import java.io.*;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

@Slf4j
public class PersistBlockChainToDiskTest {

    private static final String TEST_RESOURCE_PATH = "./src/test/resources/persistenceTest/";
    private final File directory;

    public PersistBlockChainToDiskTest() {
        Path path = Paths.get(TEST_RESOURCE_PATH);
        directory = new File(path.toUri());
    }

    @Before
    public void setUp() {
        if (!directory.isDirectory()) {
            directory.mkdir();
        }
        assertTrue(directory.isDirectory());
    }

    @After
    public void cleanUp() throws IOException {
        FileUtils.cleanDirectory(directory);
    }

    @Test
    public void shouldWriteHashBlockWithHashAsName() {
        PersistBlockChainToDisk persistenceBlockChain = new PersistBlockChainToDisk(directory);

        Block block = new FakeBlock("foo");
        HashedBlock hashedBlock = block.getHashed();
        persistenceBlockChain.write(hashedBlock);

        List<File> fileList = Arrays.asList(directory.listFiles());

        assertTrue(fileList.size() > 0);
        assertEquals(1, fileList.stream().filter(file -> file.getName().equals("foo.json")).count());
    }

    @Test
    public void shouldWriteJson() throws IOException {
        PersistBlockChainToDisk persistenceBlockChain = new PersistBlockChainToDisk(directory);

        Transaction transaction = new Transaction("", "", 0);
        Block block = new TransactionBlock("", transaction, 0);
        HashedBlock hashedBlock = block.getHashed();

        persistenceBlockChain.write(hashedBlock);
        File writtenHash = new File(directory + "/" + hashedBlock.getHash() + ".json");
        String json = readFile(writtenHash);

        log.debug(json);
        assertEquals(hashedBlock.toString(), new ObjectMapper().readValue(json, HashedBlock.class).toString());
    }

    @Test
    public void shouldReadFromFileToList() throws IOException {
        PersistBlockChainToDisk persistenceBlockChain = new PersistBlockChainToDisk(directory);

        Transaction transaction = new Transaction("", "", 0);
        Block block = new TransactionBlock("", transaction, 0);
        HashedBlock hashedBlock = block.getHashed();

        String json = new ObjectMapper().writeValueAsString(hashedBlock);
        log.debug(json);
        write(json);

        List<HashedBlock> hashedBlocks = persistenceBlockChain.read();

        assertEquals(1, hashedBlocks.size());
        assertEquals(hashedBlock.toString(), hashedBlocks.get(0).toString());
    }

    @Test(expected = ObjectNotSerializable.class)
    public void shouldThrowExceptionWhenTheJsonIsNotValid() {
        PersistBlockChainToDisk persistenceBlockChain = new PersistBlockChainToDisk(directory);
        write("foo");

        try {
            persistenceBlockChain.read();
            fail();
        } catch (ObjectNotSerializable e) {
            assertEquals("An error happened trying to serialize the json string", e.getMessage());
            throw e;
        }

    }

    private void write(String content) {
        File myFile = new File(directory, "deserializeTest.json");

        try(FileOutputStream fileOutputStream = new FileOutputStream(myFile);
            DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream)) {

            dataOutputStream.write(content.getBytes());

            log.debug("writing HashedBlock to JSON file: " + "deserializeTest.json");

            dataOutputStream.flush();
            fileOutputStream.flush();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    private String readFile(File file) {
        try (FileReader fileReader = new FileReader(file);
             BufferedReader bufferedReader = new BufferedReader(fileReader)){

            StringBuilder stringBuilder = new StringBuilder();
            String line = bufferedReader.readLine();

            while (line != null) {
                stringBuilder.append(line);
                stringBuilder.append(System.lineSeparator());
                line = bufferedReader.readLine();
            }
            return stringBuilder.toString();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private class FakeBlock implements Block {
        private final String hash;

        FakeBlock(String hash) {
            this.hash = hash;
        }

        @Override
        public String getDataToHash() {
            return "null";
        }

        @JsonIgnore
        @Override
        public HashedBlock getHashed() {
            return new HashedBlock(this, hash);
        }

        @Override
        public String getPreviousHash() {
            return "null";
        }

        @Override
        public Transaction getData() {
            return null;
        }

        @Override
        public long getTimestamp() {
            return 0;
        }
    }
}
