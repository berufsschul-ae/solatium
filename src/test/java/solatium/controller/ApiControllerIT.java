package solatium.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import solatium.Main;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = Main.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiControllerIT {

    @Autowired
    private MockMvc mvc;

    @Test
    @WithMockUser()
    public void shouldReturnTheBalance() throws Exception {
        mvc.perform(get("/api/balance"))
                .andExpect(status().isOk()).andExpect(content().string("0"));
    }

    @Test
    @WithMockUser()
    public void shouldSendTransaction() throws Exception {
        mvc.perform(post("/api/transaction")
                .param("receiverId", "user")
                .param("amount", "0"))
                .andExpect(status().isOk());

        mvc.perform(post("/api/disburse")
                .param("amount", "0"))
                .andExpect(status().isOk());

        mvc.perform(post("/api/deposit")
                .param("amount", "0"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser()
    public void shouldFailTransactionWithAmountNotBeingANumber() throws Exception {
        mvc.perform(post("/api/deposit")
                .param("amount", "foo"))
                .andExpect(status().is(400));

        mvc.perform(post("/api/disburse")
                .param("amount", "foo"))
                .andExpect(status().is(400));

        mvc.perform(post("/api/transaction")
                .param("receiverId", "user")
                .param("amount", "foo"))
                .andExpect(status().is(400));
    }

    @Test
    @WithMockUser()
    public void TransactionShouldReturn400ByMissingParams() throws Exception {
        mvc.perform(post("/api/transaction")
                .param("amount", "0"))
                .andExpect(status().is(400));

        mvc.perform(post("/api/transaction")
                .param("receiverId", "user"))
                .andExpect(status().is(400));

        mvc.perform(post("/api/transaction"))
                .andExpect(status().is(400));
    }
}