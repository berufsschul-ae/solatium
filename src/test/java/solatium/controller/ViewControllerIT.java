package solatium.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import solatium.Main;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(classes = Main.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ViewControllerIT {

    @Autowired
    private MockMvc mvc;

    @Test
    @WithMockUser()
    public void shouldGetWebPages() throws Exception {
        mvc.perform(get("/"))
                .andExpect(status().isOk());

        mvc.perform(get("/transaction"))
                .andExpect(status().isOk());

        mvc.perform(get("/disburse"))
                .andExpect(status().isOk());

        mvc.perform(get("/deposit"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser()
    public void shouldPostTransaction() throws Exception {
        mvc.perform(post("/transaction")
                .param("receiverId", "user")
                .param("amount", "0"))
                .andExpect(status().isOk());

        mvc.perform(post("/disburse")
                .param("amount", "0"))
                .andExpect(status().isOk());

        mvc.perform(post("/deposit")
                .param("amount", "0"))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser()
    public void shouldFailTransaction() throws Exception {
        mvc.perform(post("/transaction")
                .param("amount", "0"))
                .andExpect(status().is(400));

        mvc.perform(post("/transaction")
                .param("receiverId", "user"))
                .andExpect(status().is(400));

        mvc.perform(post("/transaction"))
                .andExpect(status().is(400));

        mvc.perform(post("/transaction")
                .param("amount", "okay"))
                .andExpect(status().is(400));
    }

    @Test
    @WithMockUser()
    public void shouldFailDeposit() throws Exception {
        mvc.perform(post("/disburse")
                .param("amount", ""))
                .andExpect(status().is(400));
    }

    @Test
    @WithMockUser()
    public void shouldFailDisburse() throws Exception {
        mvc.perform(post("/disburse")
                .param("amount", ""))
                .andExpect(status().is(400));
    }
}
