package solatium.interfaces;

import org.junit.Test;
import solatium.blockchain.BlockChainService;
import solatium.blockchain.Chain;

import static org.junit.Assert.assertEquals;

public class BankTest {

    @Test
    public void shouldConstructCorrect() {
        User bank = new Bank(new BlockChainService());
        assertEquals("Bank", bank.getUsername());
        assertEquals("", bank.getPassword());
        assertEquals(0, bank.getBalance());
    }

    @Test
    public void shouldSendEvenIfBalanceNegative() {
        Chain blockChain = new BlockChainService();
        User bank = new Bank(blockChain);
        assertEquals(0, bank.getBalance());
        bank.send("foo", 100);
        assertEquals(-100, bank.getBalance());

        bank.send("foo", 100);
        assertEquals(-200, bank.getBalance());
    }

}
