package solatium.interfaces;

import org.junit.Test;
import solatium.blockchain.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

public class BalanceTest {

    @Test
    public void shouldConstructCorrectly() {
        Chain blockChain = new BlockChainService();
        Balance balance = new Balance("xxx", blockChain);
        assertEquals(0, balance.getBalance());
    }

    @Test
    public void shouldUpdateBalanceWithNegativeBalance() {
        Transaction transaction0 = new Transaction("xxx", "fff", 10);
        FakeChain fakeChain = new FakeChain(transaction0);
        fakeChain.addToChain();
        Balance balance = new Balance("xxx", fakeChain);
        assertEquals(0, balance.getBalance());
        assertEquals(-10, balance.updateBalance().getBalance());
    }

    @Test
    public void shouldUpdateBalanceWithPositiveBalance() {
        Transaction transaction0 = new Transaction("fff", "xxx", 10);
        FakeChain fakeChain = new FakeChain(transaction0);
        fakeChain.addToChain();
        Balance balance = new Balance("xxx", fakeChain);
        assertEquals(0, balance.getBalance());
        assertEquals(10, balance.updateBalance().getBalance());
    }

    @Test
    public void shouldUpdateBalanceBalance() {
        Transaction transactionReceive10 = new Transaction("fff", "xxx", 10);
        Transaction transactionSend5 = new Transaction("xxx", "fff", 5);
        List<Transaction> transactions = Arrays.asList(transactionReceive10, transactionSend5);

        FakeChain fakeChain = new FakeChain(transactions);
        fakeChain.addToChain();

        Balance balance = new Balance("xxx", fakeChain);
        assertEquals(0, balance.getBalance());
        assertEquals(5, balance.updateBalance().getBalance());
    }

    private class FakeChain implements Chain {

        private final List<Transaction> transactionList;
        private final List<HashedBlock> blockChain = new ArrayList<>();

        FakeChain(List<Transaction> transactionList) {
            this.transactionList = transactionList;
        }

        FakeChain(Transaction transaction) {
            this(Collections.singletonList(transaction));
        }

        void addToChain() {
            for (Transaction transaction : transactionList) {
                Block block = new TransactionBlock("xxx", transaction, 0);
                blockChain.add(block.getHashed());
            }
        }

        @Override
        public void addToChain(HashedBlock block) {

        }

        @Override
        public void addToChain(Transaction data) {

        }

        @Override
        public boolean validateChain() {
            return false;
        }

        @Override
        public String getLastHash() {
            return null;
        }

        @Override
        public List<HashedBlock> getBlockChain() {
            return blockChain;
        }

        @Override
        public HashedBlock getGenesisBlock() {
            return new HashedBlock(new TransactionBlock("", new Transaction("","", 0), 0L),"");
        }
    }

}
