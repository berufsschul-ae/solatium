package solatium.interfaces;

import org.junit.Test;
import solatium.blockchain.BlockChainService;
import solatium.blockchain.Chain;

import static org.junit.Assert.*;

public class CustomerTest {

    private static final Chain BLOCK_CHAIN = new BlockChainService();

    @Test
    public void shouldUpdateBalance() {
        Chain blockChain = new BlockChainService();
        User bank = new Bank(blockChain);
        User customer = new Customer("username", "password", blockChain);
        assertEquals(0, customer.getBalance());
        bank.send("username", 100);
        assertEquals(100, customer.getBalance());
    }

    @Test(expected = NotEnoughMoneyException.class)
    public void shouldNotSendIfNotEnoughMoney() {
        User customer = new Customer("username", "password", BLOCK_CHAIN);
        assertEquals(0, customer.getBalance());

        try {
            customer.send("foo", 100);
            fail();
        } catch (NotEnoughMoneyException e) {
            assertEquals("You don't have enough money to make that transaction", e.getMessage());
            throw e;
        }
    }
}