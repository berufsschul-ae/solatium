package solatium;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import solatium.blockchain.BlockChainService;
import solatium.blockchain.Chain;
import solatium.blockchain.HashedBlock;
import solatium.blockchain.persistence.LoadBlockChain;
import solatium.blockchain.persistence.PersistBlockChain;
import solatium.blockchain.persistence.PersistBlockChainToDisk;

import java.io.File;
import java.util.List;

@Configuration
public class AppConfig {

    @Bean
    public Chain blockChainService() {
        File theDir = new File("./persistence/");
        if (!theDir.exists()) {
            theDir.mkdir();
        }
        PersistBlockChain persistBlockChain = new PersistBlockChainToDisk(theDir);

        List<HashedBlock> hashedBlockList = new LoadBlockChain(persistBlockChain).load();

        return new BlockChainService(hashedBlockList, persistBlockChain);
    }
}
