package solatium.auth;

import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import solatium.db.CustomerRepository;
import solatium.interfaces.Customer;

@Slf4j
@Service
public class UserPrincipalDetailsService implements UserDetailsService {

    private final CustomerRepository customerRepository;

    public UserPrincipalDetailsService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String s) {
        Customer user = this.customerRepository.findByUsername(s);

        if (user == null) {
            log.debug(String.format("User %s not found", s));
            throw new UsernameNotFoundException(String.format("User %s not found", s));
        }

        return new UserPrincipal(user);
    }
}
