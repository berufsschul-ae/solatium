package solatium.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import solatium.blockchain.Chain;
import solatium.blockchain.Transaction;
import solatium.db.CustomerRepository;
import solatium.interfaces.Customer;
import solatium.interfaces.User;

import java.security.Principal;

@Slf4j
@RestController
@RequestMapping(value = "/api")
public class ApiController {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    private Chain blockChainService;

    @Autowired
    private User bank;

    @GetMapping(value = "/balance")
    public int balance(Principal principal) {
        String username = principal.getName();
        Customer user = customerRepository.findByUsername(username);
        user.setBlockChainService(blockChainService);
        log.info("User {} has checked for his balance", username);
        return user.getBalance();
    }

    @PostMapping("/deposit")
    public void deposit(Principal principal, @RequestParam int amount) {
        String username = principal.getName();
        Customer user = customerRepository.findByUsername(username);
        bank.send(user.getUsername(), amount);
    }

    @PostMapping("/transaction")
    public void postTransaction(Principal principal, @RequestParam String receiverId, @RequestParam int amount) {
        String userName = principal.getName();
        Customer customer = customerRepository.findByUsername(userName);

        //make the transaction
        Transaction transaction = new Transaction(customer.getUsername(), receiverId, amount);
        log.info("New Transaction " + transaction);

        blockChainService.addToChain(transaction);
    }

    @PostMapping("/disburse")
    public void postDisburse(Principal principal, @RequestParam int amount) {
        String userName = principal.getName();
        Customer customer = customerRepository.findByUsername(userName);

        Transaction transaction = new Transaction(customer.getUsername(), bank.getUsername(), amount);

        blockChainService.addToChain(transaction);
    }
}
