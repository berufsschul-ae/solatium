package solatium.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import solatium.blockchain.Chain;
import solatium.blockchain.Transaction;
import solatium.db.CustomerRepository;
import solatium.interfaces.Customer;
import solatium.interfaces.User;

import java.security.Principal;

@Slf4j
@Controller("/")
public class ViewController {

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    private Chain blockChainService;

    @Autowired
    private User bank;

    @GetMapping("")
    public String index(Principal principal, Model model) {
        String userName = principal.getName();

        Customer customer = customerRepository.findByUsername(userName);
        customer.setBlockChainService(blockChainService);

        model.addAttribute("balance", customer.getBalance());

        return "index";
    }

    @GetMapping("transaction")
    public String transaction(Principal principal, Model model) {
        String userName = principal.getName();

        Customer customer = customerRepository.findByUsername(userName);

        model.addAttribute("senderId", customer.getUsername());

        return "transaction";
    }

    @PostMapping("transaction")
    public String postTransaction(Principal principal, Model model, @RequestParam String receiverId, @RequestParam int amount) {
        String userName = principal.getName();
        Customer customer = customerRepository.findByUsername(userName);

        //make the transaction
        Transaction transaction = new Transaction(customer.getUsername(), receiverId, amount);
        log.info("New Transaction " + transaction);

        blockChainService.addToChain(transaction);

        model.addAttribute("senderId", customer.getUsername());
        model.addAttribute("response", "Transaction send!");

        return "transaction";
    }

    @GetMapping("disburse")
    public String disburse(Principal principal, Model model) {
        return "disburse";
    }

    @PostMapping("disburse")
    public String postDisburse(Principal principal, Model model, @RequestParam int amount) {
        String userName = principal.getName();
        Customer customer = customerRepository.findByUsername(userName);

        Transaction transaction = new Transaction( customer.getUsername(), bank.getUsername(), amount);
        log.info("New Transaction " + transaction);

        blockChainService.addToChain(transaction);

        model.addAttribute("response", "Money disbursed. Have a nice day!");

        return "disburse";
    }

    @GetMapping("deposit")
    public String deposit(Principal principal, Model model) {
        return "deposit";
    }

    @PostMapping("deposit")
    public String postDeposit(Principal principal, Model model, @RequestParam int amount) {
        String userName = principal.getName();
        Customer customer = customerRepository.findByUsername(userName);

        Transaction transaction = new Transaction(bank.getUsername(), customer.getUsername(), amount);
        log.info("New Transaction " + transaction);

        blockChainService.addToChain(transaction);

        model.addAttribute("response", "Money deposited. Have a nice day!");

        return "deposit";
    }
}
