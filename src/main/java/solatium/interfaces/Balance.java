package solatium.interfaces;

import solatium.blockchain.Chain;
import solatium.blockchain.HashedBlock;

import java.util.List;

public class Balance {

    private final String address;
    private final Chain blockChain;
    private final int balance;

    private Balance(String user, Chain blockChain, int balanceValue) {
        this.address = user;
        this.blockChain = blockChain;
        this.balance = balanceValue;
    }

    public Balance(String address, Chain blockChain) {
        this(address, blockChain, 0);
    }

    public Balance updateBalance() {
        List<HashedBlock> hashedBlockList = blockChain.getBlockChain();
        int receiveValue = hashedBlockList.stream()
                .filter(hashedBlock -> hashedBlock.getBlock().getData().getReceiverName().equals(address))
                .mapToInt(hashedBlock -> hashedBlock.getBlock().getData().getValue()).sum();

        int sendValue = hashedBlockList.stream()
                .filter(hashedBlock -> hashedBlock.getBlock().getData().getSenderName().equals(address))
                .mapToInt(hashedBlock -> hashedBlock.getBlock().getData().getValue()).sum();

        int newBalance = receiveValue - sendValue;
        return new Balance(address, blockChain, newBalance);
    }

    public int getBalance() {
        return balance;
    }
}
