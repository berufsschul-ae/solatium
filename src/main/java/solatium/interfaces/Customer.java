package solatium.interfaces;

import lombok.Getter;
import solatium.blockchain.Chain;
import solatium.blockchain.Transaction;

import javax.persistence.*;

@Getter
@Entity
public class Customer implements User {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private String username;

    @Column(nullable = false)
    private String password;

    @Transient
    private Chain blockChainService;

    public void setBlockChainService(Chain blockChainService) {
        this.blockChainService = blockChainService;
    }

    public Customer(String username, String password, Chain blockChainService) {
        this.username = username;
        this.password = password;
        this.blockChainService = blockChainService;
    }

    protected Customer() {
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public int getBalance() {
        Balance balance = new Balance(username, blockChainService).updateBalance();
        return balance.getBalance();
    }

    @Override
    public void send(String receiverAddress, int amount) {
        Balance balance = new Balance(username, blockChainService);
        if (amount > balance.updateBalance().getBalance()) {
            throw new NotEnoughMoneyException("You don't have enough money to make that transaction");
        } else {
            Transaction transaction = new Transaction(username, receiverAddress, amount);
            blockChainService.addToChain(transaction);
        }
    }
}
