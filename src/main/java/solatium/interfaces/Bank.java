package solatium.interfaces;

import org.springframework.stereotype.Component;
import solatium.blockchain.Chain;
import solatium.blockchain.Transaction;

@Component
public class Bank implements User {

    private final Chain blockChain;

    private static final String USER_NAME = "Bank";

    public Bank(Chain blockChain) {
        this.blockChain = blockChain;
    }

    @Override
    public String getUsername() {
        return USER_NAME;
    }

    @Override
    public String getPassword() {
        return "";
    }

    @Override
    public int getBalance() {
        Balance balance = new Balance(USER_NAME, blockChain).updateBalance();
        return balance.getBalance();
    }

    @Override
    public void send(String receiverAddress, int amount) {
        Transaction transaction = new Transaction(USER_NAME, receiverAddress, amount);
        blockChain.addToChain(transaction);
    }
}
