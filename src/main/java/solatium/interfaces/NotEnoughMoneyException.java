package solatium.interfaces;

class NotEnoughMoneyException extends RuntimeException {

    public NotEnoughMoneyException(String s) {
        super(s);
    }
}
