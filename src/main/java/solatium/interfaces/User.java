package solatium.interfaces;

public interface User {

    String getUsername();

    String getPassword();

    int getBalance();

    void send(String receiverAddress, int amount);
}
