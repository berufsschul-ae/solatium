package solatium.db;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import solatium.blockchain.Chain;
import solatium.interfaces.Customer;

import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class DbInit implements CommandLineRunner {

    private final CustomerRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    private final Chain blockChainService;

    public DbInit(CustomerRepository userRepository, PasswordEncoder passwordEncoder, Chain blockChainService) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.blockChainService = blockChainService;
    }

    @Override
    public void run(String... args) {
        // Delete all
        this.userRepository.deleteAll();

        //String username, String password, String address, int errorCounter, Chain blockChainService

        // Crete users
        Customer user = new Customer("user", passwordEncoder.encode("foo"),  blockChainService);
        Customer user1 = new Customer("user1", passwordEncoder.encode("bar"), blockChainService);
        Customer augusto = new Customer("augusto", passwordEncoder.encode("augusto123"), blockChainService);
        Customer blazej = new Customer("blazej", passwordEncoder.encode("blazej123"), blockChainService);
        Customer bjarne = new Customer("bjarne", passwordEncoder.encode("bjarne123"), blockChainService);
        Customer bank = new Customer("Bank", passwordEncoder.encode("Bank123"), blockChainService);
        Customer heiko = new Customer("heiko", passwordEncoder.encode("heiko123"), blockChainService);


        List<Customer> users = Arrays.asList(user, user1, augusto, bjarne, blazej, bank, heiko);

        // Save to db
        this.userRepository.saveAll(users);

        log.info(userRepository.findByUsername("user").getUsername());
    }

}
