package solatium.blockchain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public final class HashedBlock {

    private final String hash;
    private final Block block;

    @JsonCreator
    public HashedBlock(@JsonProperty("block") Block block, @JsonProperty("hash") String hash) {
        this.hash = hash;
        this.block = block;
    }

    @Override
    public String toString() {
        return "HashedBlock{" +
                "hash='" + hash + '\'' +
                ", block=" + block +
                '}';
    }
}
