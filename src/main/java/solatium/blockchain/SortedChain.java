package solatium.blockchain;

import java.util.ArrayList;
import java.util.List;

public class SortedChain {

    private static final String GENESISHASH = "0";
    private final List<HashedBlock> unsortedHashBlockList;

    public SortedChain(List<HashedBlock> unsortedHashBlockList) {
        this.unsortedHashBlockList = unsortedHashBlockList;
    }

    // das ist nicht perfomant, aber es reicht aus
    public List<HashedBlock> sort() {

        List<HashedBlock> sortedList = new ArrayList<>();

        String currentHash = GENESISHASH;

        for(int i = 0; i < unsortedHashBlockList.size(); i++) {
            HashedBlock foundHashedBlock = searchHashedBlockByPreviousHash(currentHash);

            sortedList.add(foundHashedBlock);
            currentHash = foundHashedBlock.getHash();

        }

        return sortedList;
    }

    private HashedBlock searchHashedBlockByPreviousHash(String hash) {

        for(HashedBlock hashedBlock: unsortedHashBlockList) {
            if(hashedBlock.getBlock().getPreviousHash().equals(hash)) {
                return hashedBlock;
            }
        }

        return unsortedHashBlockList.get(0);
    }


}
