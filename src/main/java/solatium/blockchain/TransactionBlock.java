package solatium.blockchain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public final class TransactionBlock implements Block {

    private final String previousHash;
    private final Transaction data;
    private final long timestamp;

    @JsonCreator
    public TransactionBlock(@JsonProperty("previousHash") String previousHash, @JsonProperty("data") Transaction data, @JsonProperty("timestamp") long timestamp) {
        this.previousHash = previousHash;
        this.data = data;
        this.timestamp = timestamp;
    }

    @JsonIgnore
    @Override
    public String getDataToHash() {
        return data.toString() + timestamp;
    }

    @JsonIgnore
    @Override
    public HashedBlock getHashed() {
        return new HashedBlock(this, new Sha256Hash(getDataToHash(), previousHash).getHash());
    }

    @Override
    public String toString() {
        return "Block{" +
                "previousHash='" + previousHash + '\'' +
                ", data='" + data + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }
}
