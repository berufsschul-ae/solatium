package solatium.blockchain;


import solatium.blockchain.persistence.PersistBlockChain;
import solatium.blockchain.persistence.PersistBlockChainToVoid;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public final class BlockChainService implements Chain {

    private static final String GENESIS_HASH = "0";
    private final List<HashedBlock> blockChain;
    private final PersistBlockChain persistBlockChain;

    public BlockChainService() {
        blockChain = new ArrayList<>();
        Transaction genesisTransaction = new Transaction("0", "0", 0);
        Block genesisBlock = new TransactionBlock(GENESIS_HASH, genesisTransaction, 0);
        HashedBlock hashedGenesisBlock = genesisBlock.getHashed();
        blockChain.add(hashedGenesisBlock);
        persistBlockChain = new PersistBlockChainToVoid();
    }

    BlockChainService(List<HashedBlock> blockChain) {
        this(blockChain, new PersistBlockChainToVoid());
    }

    public BlockChainService(List<HashedBlock> blockChain, PersistBlockChain persistBlockChain) {
        this.persistBlockChain = persistBlockChain;
        this.blockChain = blockChain;

        if (blockChain.isEmpty()) {
            Transaction genesisTransaction = new Transaction("0", "0", 0);
            Block genesisBlock = new TransactionBlock(GENESIS_HASH, genesisTransaction, 0);
            HashedBlock hashedGenesisBlock = genesisBlock.getHashed();
            addToChain(hashedGenesisBlock);
        } else if (!blockChain.get(0).getBlock().getPreviousHash().equals(GENESIS_HASH)) {
            throw new BlockChainStartsNotWithGenesisBlockException();
        }

    }

    @Override
    public void addToChain(HashedBlock block) {
        blockChain.add(block);
        persistBlockChain.write(block);
    }

    @Override
    public void addToChain(Transaction data) {
        Block block = new TransactionBlock(getLastHash(), data, new Date().getTime());
        HashedBlock hashedBlock = block.getHashed();
        addToChain(hashedBlock);
        persistBlockChain.write(hashedBlock);
    }

    @Override
    public boolean validateChain() {
        Iterator<HashedBlock> hashedBlockIterator = blockChain.iterator();
        String previousHash = GENESIS_HASH;

        while (hashedBlockIterator.hasNext()) {
            HashedBlock hashedBlock = hashedBlockIterator.next();
            String originalHash = hashedBlock.getHash();
            Block block = hashedBlock.getBlock();
            String newHash = new Sha256Hash(block.getDataToHash(), previousHash).getHash();
            if (!originalHash.equals(newHash) || !block.getPreviousHash().equals(previousHash)) {
                return false;
            }
            previousHash = originalHash;
        }

        return true;
    }

    @Override
    public String getLastHash() {
        return blockChain.get(blockChain.size() - 1).getHash();
    }

    @Override
    public List<HashedBlock> getBlockChain() {
        List<HashedBlock> blockChainList = new ArrayList<>();

        blockChain.iterator().forEachRemaining(blockChainList::add);
        return blockChainList;
    }

    @Override
    public HashedBlock getGenesisBlock() {
        return blockChain.get(0);
    }

    @Override
    public String toString() {
        List<HashedBlock> hashedBlocks = getBlockChain();
        StringBuilder hashedBlocksString = new StringBuilder();
        hashedBlocks.forEach(hashedBlock -> hashedBlocksString.append(hashedBlock.toString()).append("\n"));
        return "Chain{\n" +
                hashedBlocksString.toString() +
                '}';

    }
}
