package solatium.blockchain;

import lombok.extern.slf4j.Slf4j;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Slf4j
public final class Sha256Hash implements Hash {

    private final String input;

    public Sha256Hash(String data, String previousHash) {
        this.input = data + previousHash;
    }

    @Override
    public String getHash(){
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(input.getBytes(StandardCharsets.UTF_8));

            return DatatypeConverter.printHexBinary(hash);
        }
        catch(NoSuchAlgorithmException e) {
            log.error(e.toString());
            throw new IllegalStateException(e);
        }
    }

}
