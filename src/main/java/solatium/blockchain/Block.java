package solatium.blockchain;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonDeserialize(as = TransactionBlock.class)
public interface Block {
    String getDataToHash();

    HashedBlock getHashed();

    @Override
    String toString();

    String getPreviousHash();

    Transaction getData();

    long getTimestamp();
}
