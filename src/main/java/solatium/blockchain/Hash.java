package solatium.blockchain;

interface Hash {

    String getHash();
}
