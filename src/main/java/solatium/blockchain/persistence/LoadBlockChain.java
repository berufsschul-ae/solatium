package solatium.blockchain.persistence;

import solatium.blockchain.BlockChainService;
import solatium.blockchain.Chain;
import solatium.blockchain.HashedBlock;
import solatium.blockchain.SortedChain;

import java.util.List;

public class LoadBlockChain {

    private final PersistBlockChain persistBlockChain;

    public LoadBlockChain(PersistBlockChain persistBlockChain) {
        this.persistBlockChain = persistBlockChain;
    }

    public List<HashedBlock> load() {
        List<HashedBlock> hashedBlockList = persistBlockChain.read();
        List<HashedBlock> sortedHashedBlockList = new SortedChain(hashedBlockList).sort();
        Chain blockChainService = new BlockChainService(sortedHashedBlockList, persistBlockChain);
        if (!blockChainService.validateChain()) {
            throw new BlockChainValidationException("The block chain is corrupted");
        }
        return sortedHashedBlockList;
    }
}
