package solatium.blockchain.persistence;

import lombok.extern.slf4j.Slf4j;
import solatium.blockchain.HashedBlock;

import java.util.ArrayList;
import java.util.List;

@Slf4j
public class PersistBlockChainToVoid implements PersistBlockChain {

    @Override
    public void write(HashedBlock hashedBlock) {
        log.debug("writing to void: {}", hashedBlock);
    }

    @Override
    public List<HashedBlock> read() {
        log.debug("returning empty list");
        return new ArrayList<>();
    }
}
