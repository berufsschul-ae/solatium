package solatium.blockchain.persistence;

public class BlockChainValidationException extends RuntimeException {

    public BlockChainValidationException(String message) {
        super(message);
    }
}
