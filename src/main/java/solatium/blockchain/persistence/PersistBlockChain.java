package solatium.blockchain.persistence;

import solatium.blockchain.HashedBlock;

import java.util.List;

public interface PersistBlockChain {
    void write(HashedBlock hashedBlock);

    List<HashedBlock> read();
}
