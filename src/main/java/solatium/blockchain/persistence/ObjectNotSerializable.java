package solatium.blockchain.persistence;

class ObjectNotSerializable extends RuntimeException {

    public ObjectNotSerializable(String message) {
        super(message);
    }
}
