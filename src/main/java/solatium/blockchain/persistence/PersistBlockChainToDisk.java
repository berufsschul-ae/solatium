package solatium.blockchain.persistence;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import solatium.blockchain.HashedBlock;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;

@Slf4j
public class PersistBlockChainToDisk implements PersistBlockChain {

    private final File blockChainFileDirectory;
    private final ObjectMapper mapper;

    public PersistBlockChainToDisk(File blockChainFileDirectory) {
        this.blockChainFileDirectory = blockChainFileDirectory;
        mapper = new ObjectMapper();
    }

    @Override
    public void write(HashedBlock hashedBlock) {
        String fileName = hashedBlock.getHash() + ".json";
        File myFile = new File(blockChainFileDirectory, fileName);

        try (FileOutputStream fileOutputStream = new FileOutputStream(myFile);
             DataOutputStream dataOutputStream = new DataOutputStream(fileOutputStream)) {

            String hashedBlockJson = mapper.writeValueAsString(hashedBlock);
            dataOutputStream.write(hashedBlockJson.getBytes());

            log.debug("writing HashedBlock to JSON file: " + fileName);

            dataOutputStream.flush();
            fileOutputStream.flush();
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<HashedBlock> read() {
        List<HashedBlock> hashedBlockList = new ArrayList<>();
        File[] files = blockChainFileDirectory.listFiles();

        for (File file : files) {
            try (FileReader fileReader = new FileReader(file);
                 BufferedReader bufferedReader = new BufferedReader(fileReader)) {

                StringBuilder stringBuilder = new StringBuilder();
                String line = bufferedReader.readLine();

                while (line != null) {
                    stringBuilder.append(line);
                    stringBuilder.append(System.lineSeparator());
                    line = bufferedReader.readLine();
                }

                String json = stringBuilder.toString();

                try {
                    HashedBlock hashedBlock = new ObjectMapper().readValue(json, HashedBlock.class);
                    hashedBlockList.add(hashedBlock);
                } catch (IOException e) {
                    throw new ObjectNotSerializable("An error happened trying to serialize the json string");
                }
            } catch (IOException e) {
                throw new MissingResourceException("Error while reading File", PersistBlockChainToDisk.class.toString(), blockChainFileDirectory.getPath());
            }
        }

        return hashedBlockList;
    }
}
