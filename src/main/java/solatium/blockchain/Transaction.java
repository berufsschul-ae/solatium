package solatium.blockchain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

@Getter
public class Transaction {

    private final String senderName;
    private final String receiverName;
    private final int value;

    @JsonCreator
    public Transaction(@JsonProperty("senderName") String senderName, @JsonProperty("receiverName") String receiverName, @JsonProperty("value") int value) {
        this.senderName = senderName;
        this.receiverName = receiverName;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "senderName='" + senderName + '\'' +
                ", receiverName='" + receiverName + '\'' +
                ", value=" + value +
                '}';
    }
}
