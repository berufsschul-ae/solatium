package solatium.blockchain;

import java.util.List;

public interface Chain {

    void addToChain(HashedBlock block);

    void addToChain(Transaction data);

    boolean validateChain();

    String getLastHash();

    List<HashedBlock> getBlockChain();

    HashedBlock getGenesisBlock();

    @Override
    String toString();
}
