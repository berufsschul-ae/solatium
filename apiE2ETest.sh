#!/usr/bin/env bash
# THIS IS API TEST

# start the server
echo "Startin Spring-Boot Server"
mvn spring-boot:run -Dmaven.test.skip=true 2>&1 >/dev/null &
PROCESS_ID=$!

# Loop for checking if server is running
until nc -z -v -w30 "localhost" "8080"
do
  echo "Waiting for Spring-Boot connection..."
  sleep 20
done

# request function
request () {
	curl $3 -s -u $2 localhost:8080/api/$1
}

# assert function
assertEqual() {
    if [ "$1" = "$2" ]; then
      echo "$1 has the value $2"
    else
        echo "$1 has not the value $2"
        kill -1 $PROCESS_ID
        exit 1
    fi
}

nextTest() {
    unset x
    echo ""
    echo "##################"
    echo ""
}

echo "Testing Rest - API"

USER="user:foo"
USER1="user1:bar"

echo "checking for balance of user should be 0"
x=$(request balance $USER)
assertEqual $x "0"

nextTest
echo "sending transaction from user to user1 with amount 100"
request "transaction?receiverId=user1&amount=100" $USER "-X POST"

echo "checking for balance of user should be -100"
x=$(request balance $USER)
assertEqual $x "-100"

nextTest
echo "checking for balance of user1 should be 100"
x=$(request balance $USER1)
assertEqual $x "100"

nextTest
echo "Disbursing from user1 amount 100"
request "disburse?amount=100" $USER1 "-X POST"

echo "checking for balance of user1 should be 0"
x=$(request balance $USER1)
assertEqual $x "0"

nextTest
echo "Deposit from user amount 100"
request "deposit?amount=100" $USER "-X POST"

echo "checking for balance of user should be 0"
x=$(request balance $USER)
assertEqual $x "0"

nextTest
echo "All tests successful!"

kill -1 $PROCESS_ID
